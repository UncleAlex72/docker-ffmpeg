FROM eclipse-temurin:17-alpine

MAINTAINER Alex Jones <alex.jones@unclealex.co.uk>

ENV FFMPEG_VERSION 7.1

## Build dependencies.
RUN	apk update && apk add	\
  gcc tzdata binutils build-base libgcc make pkgconf pkgconfig \
  ca-certificates pcre git bash \
  musl-dev libc-dev pcre-dev zlib-dev \
  nasm yasm-dev lame-dev libogg-dev libvorbis-dev && \
  echo http://dl-cdn.alpinelinux.org/alpine/edge/testing >> /etc/apk/repositories && \
  apk add --update fdk-aac-dev && \
  cd /tmp/ && wget http://ffmpeg.org/releases/ffmpeg-${FFMPEG_VERSION}.tar.gz && \
  tar zxf ffmpeg-${FFMPEG_VERSION}.tar.gz && rm ffmpeg-${FFMPEG_VERSION}.tar.gz  && \
  cd /tmp/ffmpeg-${FFMPEG_VERSION} && \
  ./configure \
    --disable-ffplay \
    --disable-ffprobe \
    --disable-doc \
    --enable-version3 \
    --disable-gpl \
    --enable-nonfree \
    --enable-small \
    --enable-libmp3lame \
    --enable-libvorbis \
    --enable-libfdk-aac \
    --enable-postproc \
    --disable-debug && \
  make && \
  make install && \
  rm -rf /var/cache/* /tmp/*
